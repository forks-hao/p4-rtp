## Ejercicio 3. Análisis general
* ¿Cuántos paquetes componen la captura?: lo componen 1268 paquetes
* ¿Cuánto tiempo dura la captura?: dura 12.81 segundos
* Aproximadamente, ¿cuántos paquetes se envían por segundo?: 99 paquetes por segundos
* ¿Qué protocolos de nivel de red aparecen en la captura?: IPv4
* ¿Qué protocolos de nivel de transporte aparecen en la captura?: UDP
* ¿Qué protocolos de nivel de aplicación aparecen en la captura?: RTP
## Ejercicio 4. Análisis de un paquete
* Número aleatorio elegido (N): 400
* Dirección IP de la máquina A: 216.234.64.16
* Dirección IP de la máquina B: 192.168.0.10
* Puerto UDP desde el que se envía el paquete: 54550
* Puerto UDP hacia el que se envía el paquete: 49154
* SSRC del paquete: 0x31BE1E0E
* Tipo de datos en el paquete RTP (según indica el campo correspondiente): ITU-T G.711 PCMU(0)
## Ejercicio 5. Análisis de un flujo (stream)
* ¿Cuál es el primer número de tu DNI / NIE?: 3
* ¿Cuántos paquetes hay en el flujo F?: trae 642 paquetes
* ¿El origen del flujo F es la máquina A o B?: ES de la maquina B
* ¿Cuál es el puerto UDP de destino del flujo F?: 54550 (maquina A)
* ¿Cuántos segundos dura el flujo F?: dura 12.81 segundos
## Ejercicio 6. Métricas del flujo
* ¿Cuál es la diferencia máxima en el flujo?: la diferencia máxima es de 31.653 ms
* ¿Cuál es el jitter medio del flujo?: su flujo es de 12.234262 ms
* ¿Cuántos paquetes del flujo se han perdido?: 0, no se han perido ninguno 
* ¿Cuánto tiempo (aproximadamente) está el jitter por encima de 0.4 ms?: 12.806 segundos aproximadamente
## Ejercicio 7. Métricas de un paquete RTP
* ¿Cuál es su número de secuencia dentro del flujo RTP?: 18635
* ¿Ha llegado pronto o tarde, con respecto a cuando habría debido llegar?: pronto, con un skew de 13.845
* ¿Qué jitter se ha calculado para ese paquete?: 0.157713 ms
* ¿Qué timestamp tiene ese paquete?: tiene un timestamp de 1769337483
* ¿Por qué número hexadecimal empieza sus datos de audio?: f
## Ejercicio 8. Captura de una traza RTP
* Salida del comando `date`: dom 22 oct 2023 20:59:35 CEST
* Número total de paquetes en la captura: 5498 paquetes
* Duración total de la captura (en segundos): 14.007727segundos
## Ejercicio 9. Análisis de la captura realizada
* ¿Cuántos paquetes RTP hay en el flujo?: Hay 450 paquetes RTP en el flujo
* ¿Cuál es el SSRC del flujo?: 0x28677063
* ¿En qué momento (en ms) de la traza comienza el flujo?: En el 3617.071262 ms
* ¿Qué número de secuencia tiene el primer paquete del flujo?: 7289
* ¿Cuál es el jitter medio del flujo?: su jitter medio es 0
* ¿Cuántos paquetes del flujo se han perdido?: 0 paquetes se han perdido 
## Ejercicio 10 (segundo periodo). Analiza la captura de un compañero
* ¿De quién es la captura que estás usando?: MORENO SALOMON, YURIY ALEJANDRO
* ¿Cuántos paquetes de más o de menos tiene que la tuya? (toda la captura): tengo 2614 paquetes mas que él
* ¿Cuántos paquetes de más o de menos tiene el flujo RTP que el tuyo?: Tenemos los mismo paquetes del flujo RTP
* ¿Qué diferencia hay entre el número de secuencia del primer paquete de su flujo RTP con respecto al tuyo?: Tenemos una diferencia de 11572 en el número de secuencia.
* En general, ¿que diferencias encuentras entre su flujo RTP y el tuyo?: En general las secuencias de paquetes, valores del tiempo del flujo, las deltas, source port y el SSRC
